"use strict";

/*1. Опишіть, як можна створити новий HTML тег на сторінці.

    Метод document.createElement(tag) создает тег, но не добавляет его на страницу.
    Метод parent.innerHTML = "html" может создть и сразу добавить на страницу, во внутрь parent, нужную нам разметку.
    И метод parent.insertAdjacentHTML(position, html) также создает и сразу добавляет на страницу, в нужное нам место родительского тега, разметку.
*/

/* 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

    Первый параметр - это строка, которая указывает место для вставки HTML, относительно элемента к которому применяется данная функция.
    Существует 4 варианта позиций для добавления элемента: 
    'beforebegin' - перед элементом;
    'afterbegin' - в начале элемента, после открывающего тега;
    'beforeend' - в конце єлемента, перед закрывающим тегом;
    'afterend' - после самого элемента.
    Также метод insertAdjacentHTML не удаляет имеющиеся элементы в родительском теге, а добавляет новые к существующим. 
*/

/* 3. Як можна видалити елемент зі сторінки?

    Для удаления элемента можно применить метод element.remove(). 
    Также можно удалить все дочерние элементы какого-то элемента, присвоив ему пустую строку через element.innerHTML = ''; 
*/

const data1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const data2 = ["1", "2", 3, "sea", "user", 23, undefined, true];
const data3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const data4 = ["Яблоки", ["Кислые", ["Белый налив", "Чемпион"], "Сладкие", ["Голден", "Гала"]], "Груши", ["Все сладкие"]];

const container = document.querySelector('.container');
document.body.append(container);

const button = document.createElement('button');
button.className = "button";
button.innerHTML = `Очистить страницу`;
button.addEventListener('click', () => clearList());
container.after(button);

const createList = (arr, parent = document.body) => {
    const list = document.createElement('ul');
    list.className = "list";
    arr.forEach((el, i) => {
        const listItem = document.createElement('li');
        if (!Array.isArray(arr[i]) && !Array.isArray(arr[i + 1])) {
            listItem.innerHTML = `${el};`;
            list.append(listItem);
            return list;
        } else if (!Array.isArray(arr[i]) && Array.isArray(arr[i + 1])) {
            listItem.innerHTML = `${el}:`;
            list.append(listItem);
            return list;
        } else {
            createList(el, list);
        }
    });
    return parent.append(list);
};

const clearList = () => {
    button.setAttribute('disabled', true);
    button.classList.add("disabled");
    let count = 3;
    button.innerHTML = `Страница очистится через: ${count}c`;
    const delay = setInterval(() => button.innerHTML = `Страница очистится через: ${--count}c`, 1000);
    setTimeout(() => {
        clearInterval(delay);
        document.documentElement.innerHTML = '';
    }, 4000);
}

Array(2).fill(1).forEach(() => createList(data2, container));
Array(2).fill(1).forEach(() => createList(data1, container));
Array(2).fill(1).forEach(() => createList(data3, container));
Array(2).fill(1).forEach(() => createList(data4, container));

